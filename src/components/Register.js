
import React, { useState, useEffect } from 'react';
import { createBrowserHistory } from "history";

function Register() {
  useEffect(() => {
    if (localStorage.getItem('user-info')) {
      history.push("/dashboard");
    }
  }, [])
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const history = createBrowserHistory({ forceRefresh: true });
  async function signup() {
    let item = { name, email, password };
    // console.warn(item);
    let result = await fetch("http://localhost:8000/api/register", {
      method: 'POST',
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json"
      },
      body: JSON.stringify(item)

    });
    result = await result.json();
    console.warn("result", result);
    localStorage.setItem("user-info", JSON.stringify(result));
    history.push("/dashboard");


  }
  return (

    <div class="hold-transition register-page">
      <div className="register-box">
        <div className="card card-outline card-primary">
          <div className="card-header text-center">
            <a href="../../index2.html" className="h1"><b>Admin</b>LTE</a>
          </div>
          <div className="card-body">
            <p className="login-box-msg">Register a new membership</p>
            
              <div className="input-group mb-3">
                <input type="text" className="form-control" onChange={(e) => setName(e.target.value)} placeholder="Full name" />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-user" />
                  </div>
                </div>
              </div>
              <div className="input-group mb-3">
                <input type="email" className="form-control" onChange={(e) => setEmail(e.target.value)} placeholder="Email" />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-envelope" />
                  </div>
                </div>
              </div>
              <div className="input-group mb-3">
                <input type="password" className="form-control" onChange={(e) => setPassword(e.target.value)} placeholder="Password" />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-lock" />
                  </div>
                </div>
              </div>
              {/* <div className="input-group mb-3">
                <input type="password" className="form-control" placeholder="Retype password" />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-lock" />
                  </div>
                </div>
              </div> */}
              <div className="row">
                <div className="col-8">
                  <div className="icheck-primary">
                    <input type="checkbox" id="agreeTerms" name="terms" defaultValue="agree" />
                    <label htmlFor="agreeTerms">
                      I agree to the <a href="#">terms</a>
                    </label>
                  </div>
                </div>
                {/* /.col */}
                <div className="col-4">
                  <button type="submit" onClick={signup} className="btn btn-primary btn-block">Register</button>
                </div>
                {/* /.col */}
              </div>
            
            <div className="social-auth-links text-center">
              <a href="#" className="btn btn-block btn-primary">
                <i className="fab fa-facebook mr-2" />
                Sign up using Facebook
              </a>
              <a href="#" className="btn btn-block btn-danger">
                <i className="fab fa-google-plus mr-2" />
                Sign up using Google+
              </a>
            </div>
            <a href="login.html" className="text-center">I already have a membership</a>
          </div>
          {/* /.form-box */}
        </div>{/* /.card */}
      </div>
      {/* /.register-box */}

    </div>
  )

}
export default Register;
