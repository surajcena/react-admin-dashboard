
import { MDBDataTable } from 'mdbreact';
import React, { useState, useEffect } from 'react'

const Tabletest = () => {
  const [data, SetData] = useState([]);
  useEffect(async () => {
      getData();
  }, [])
  async function Delete(id) {
      let result = await fetch("http://localhost:8000/api/delete/" + id, {
          method: 'DELETE'
      });
      result = await result.json();
      console.warn(result);
      getData();

  }
  async function getData() {
      let result = await fetch("http://localhost:8000/api/list");
      result = await result.json();
      SetData(result);
  }
  const data1 = {
    columns: [
      {
        label: 'Id',
        field: 'id',
        sort: 'asc',
        width: 150
      },
      {
        label: 'Name',
        field: 'name',
        sort: 'asc',
        width: 270
      },
      {
        label: 'Description',
        field: 'description',
        sort: 'asc',
        width: 200
      },
      {
        label: 'Price',
        field: 'price',
        sort: 'asc',
        width: 100
      },
      {
        label: 'Image',
        field: <>
        {
      data.map((item) =>

            <img  src={"http://localhost:8000/" + item.file_path} />
      )}
        </>,
        sort: 'asc',
        width: 150
      },
      
    ],
    
    rows:
      
     data
      
    
    
    
  };

  return (
    // <>
    // {
    //   data.map((item) =>
          
    //         <td><img style={{ width: 100 }} src={"http://localhost:8000/" + item.file_path} /></td>
         
    //   )}
        
    <MDBDataTable
      striped
      bordered
      hover
      data={data1}
      
    />
      
      
      // </>
 
  );
}

export default Tabletest;