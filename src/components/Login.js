import React, { useState, useEffect } from 'react';
import { createBrowserHistory } from "history";

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const history = createBrowserHistory({ forceRefresh: true });
  useEffect(() => {
    if (localStorage.getItem('user-info')) {
      history.push("/dashboard");
    }
  }, [])

  async function login() {
    let item = { email, password };
    //API call has 2 promises
    let result = await fetch("http://localhost:8000/api/login", {
      method: 'POST',
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json"
      },
      body: JSON.stringify(item)

    });
    // above code resolve 1 promise 
    result = await result.json();   //this code resolve 2nd promise
    localStorage.setItem("user-info", JSON.stringify(result));
    history.push("/dashboard");
    // console.warn(email,password);

  }
  return (
    <div class="hold-transition register-page">
      <div className="login-box">
        {/* /.login-logo */}
        <div className="card card-outline card-primary">
          <div className="card-header text-center">
            <a href="../../index2.html" className="h1"><b>Admin</b>LTE</a>
          </div>
          <div className="card-body">
            <p className="login-box-msg">Sign in to start your session</p>
            
              <div className="input-group mb-3">
                <input type="email" className="form-control" onChange={(e) => setEmail(e.target.value)} placeholder="Email" />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-envelope" />
                  </div>
                </div>
              </div>
              <div className="input-group mb-3">
                <input type="password" className="form-control" onChange={(e) => setPassword(e.target.value)} placeholder="Password" />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-lock" />
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-8">
                  <div className="icheck-primary">
                    <input type="checkbox" id="remember" />
                    <label htmlFor="remember">
                      Remember Me
                    </label>
                  </div>
                </div>
                {/* /.col */}
                <div className="col-4">
                  <button type="submit" onClick={login} className="btn btn-primary btn-block">Sign In</button>
                </div>
                {/* /.col */}
              </div>
           
            <div className="social-auth-links text-center mt-2 mb-3">
              <a href="#" className="btn btn-block btn-primary">
                <i className="fab fa-facebook mr-2" /> Sign in using Facebook
              </a>
              <a href="#" className="btn btn-block btn-danger">
                <i className="fab fa-google-plus mr-2" /> Sign in using Google+
              </a>
            </div>
            {/* /.social-auth-links */}
            <p className="mb-1">
              <a href="forgot-password.html">I forgot my password</a>
            </p>
            <p className="mb-0">
              <a href="register.html" className="text-center">Register a new membership</a>
            </p>
          </div>
          {/* /.card-body */}
        </div>
        {/* /.card */}
      </div>
      {/* /.login-box */}


    </div>
  )
}
export default Login
