import Header from './Header';
import Menu from './Menu';
import Footer from './Footer';
import { withRouter } from 'react-router-dom'
import { useState, useEffect } from 'react'
import { createBrowserHistory } from "history";

function Updateproduct(props) {
    const [data, setData] = useState([]);
    const [name, setName] = useState("");
    const [price, setPrice] = useState("");
    const [description, setDescription] = useState("");
    const [file, setFile] = useState("");
    
    console.warn("props", props.match.params.id);
    useEffect(async () => {
        let result = await fetch("http://localhost:8000/api/product/" + props.match.params.id);
        result = await result.json();
        setData(result);
        setName(result.name);
        setPrice(result.price);
        setDescription(result.description);
        setFile(result.file);

    },[]);
    
     async function update(id) {
        const history = createBrowserHistory({ forceRefresh: true });
        // console.warn(file,name,price,description);
        const formData = new FormData();
        formData.append('file', file);
        formData.append('name', name);
        formData.append('price', price);
        formData.append('description', description);

        let result = await fetch("http://localhost:8000/api/update/"+id+"?_method=PUT", {
            method: 'POST',
            body:formData
           
        });
        history.push("/productlist");
        alert('Data has been Updated');
       
    }
    
    return (
        <div>
           
            {/* content wrapper vitra nai copy garna parxa */}
            <div className="content-wrapper">
            <Header />
            <Menu />
            <Footer />

                
                    <div className="row">
                        {/* left column */}
                        <div className="col-md-12">
                            {/* general form elements */}
                            <div className="card card-primary">
                                <div className="card-header">
                                    <h3 className="card-title">Add Product</h3>
                                </div>
                                
                                    <div className="card-body">
                                        <div className="form-group">
                                            <label>Name</label>
                                            <input type="text" onChange={(e) => setName(e.target.value)} defaultValue={data.name} className="form-control" placeholder="Name" />
                                        </div>
                                        <div className="form-group">
                                            <label>Price</label>
                                            <input type="text" onChange={(e) => setPrice(e.target.value)} defaultValue={data.price} className="form-control" placeholder="price" />
                                        </div>
                                        <div className="form-group">
                                            <label>Description</label>
                                            <input type="text" onChange={(e) => setDescription(e.target.value)} defaultValue={data.description} className="form-control" placeholder="Description" />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="exampleInputFile">File input</label>
                                            <div className="input-group">
                                                <div className="custom-file">
                                                    <input type="file" onChange={(e) => setFile(e.target.files[0])} defaultValue={data.file_path} className="custom-file-input" id="exampleInputFile" />
                                                    <label className="custom-file-label" htmlFor="exampleInputFile">Choose Image</label>

                                                </div>

                                                <div className="input-group-append">
                                                    <span className="input-group-text">Upload</span>
                                                </div>
                                            </div><br />
                                            <img style={{ width: 100 }} src={"http://localhost:8000/" + data.file_path} />
                                        </div>

                                    </div>
                                    {/* /.card-body */}
                                    <div className="card-footer">
                                        <button type="submit" onClick={()=>update(data.id)} className="btn btn-primary">Update</button>
                                    </div>
                               

                            </div></div></div></div>
            </div>


        
    )
};
export default withRouter(Updateproduct)
