import Header from './Header';
import React, { useState } from 'react';
import Menu from './Menu';
import Footer from './Footer';
// import { Button, Table } from "react-bootstrap";
function Searchproduct() {
    const [data, setData] = useState([]);
    async function search(key) {
        console.warn(key)
        let result = await fetch('http://localhost:8000/api/search/' + key);
        result = await result.json();//to make readable format from json type
        setData(result);
    }
    
    return (
        <div>
            <div className="content-wrapper">
                <Header />
                <Menu />
                <Footer />
            
                <h1>Search</h1>
                <br />
                <input type="text"  onChange={(e) => search(e.target.value)} placeholder="Search Here" className="form-control" />
                <div className="card-body table-responsive p-0" >
                                <table className="table table-head-fixed text-nowrap">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Price</th>
                                            <th>Image</th>
                                            <th>Description</th>
                                            <th>Operations</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            data.map((item) =>
                                                <tr>
                                                    <td>{item.id}</td>
                                                    <td>{item.name}</td>
                                                    <td>{item.price}</td>
                                                    <td><img style={{ width: 100 }} src={"http://localhost:8000/" + item.file_path} /></td>
                                                    <td>{item.description}</td>
                                                    {/* <td><span onClick={() => Delete(item.id)} className="delete"><i className="fas fa-trash-alt" /></span>
                                                        &nbsp;&nbsp;&nbsp;
                                                        <Link to={"update/" + item.id}>
                                                            <span className="update"><i className="fas fa-edit" /></span>
                                                        </Link></td> */}

                                                </tr>
                                            )
                                        }
                                    </tbody>
                                </table>
                            </div>

            </div>
        </div>
     
    )
}

export default Searchproduct
