import { Link } from 'react-router-dom'
import Menu from './Menu'
import Header from './Header'
import Footer from './Footer'
import React, { useState, useEffect } from 'react'
function Productlist() {
    const [data, SetData] = useState([]);
    useEffect(async () => {
        getData();
    }, [])
    async function Delete(id) {
        let result = await fetch("http://localhost:8000/api/delete/" + id, {
            method: 'DELETE'
        });
        result = await result.json();
        console.warn(result);
        getData();

    }
    async function getData() {
        let result = await fetch("http://localhost:8000/api/list");
        result = await result.json();
        SetData(result);
    }
    return (

        <div>


            {/* content wrapper vitra nai copy garna parxa */}
            <div className="content-wrapper">
                <Header />
                <Menu />
                <Footer />
                <div className="row">
                    <div className="col-12">
                        
                            <div className="card-header">
                                <h3 className="card-title">Product Table</h3>
                                <div className="card-tools">

                                    <div className="input-group input-group-sm" style={{ width: 150 }}>
                                        {/* <input type="text" name="table_search" className="form-control float-right" placeholder="Search" /> */}
                                        <div className="input-group-append">
                                            <Link to="add">
                                                <button type="button" class="btn btn-block btn-primary btn-sm">Add product
                                                    <ion-icon name="add-outline"></ion-icon></button>
                                            </Link>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/* /.card-header */}
                            <div className="card-body table-responsive p-0" >
                                <table className="table table-head-fixed text-nowrap">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Price</th>
                                            <th>Image</th>
                                            <th>Description</th>
                                            <th>Operations</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            data.map((item) =>
                                                <tr>
                                                    <td>{item.id}</td>
                                                    <td>{item.name}</td>
                                                    <td>{item.price}</td>
                                                    <td><img style={{ width: 100 }} src={"http://localhost:8000/" + item.file_path} /></td>
                                                    <td>{item.description}</td>
                                                    <td><span onClick={() => Delete(item.id)} className="delete"><i className="fas fa-trash-alt" /></span>
                                                        &nbsp;&nbsp;&nbsp;
                                                        <Link to={"update/" + item.id}>
                                                            <span className="update"><i className="fas fa-edit" /></span>
                                                        </Link></td>

                                                </tr>
                                            )
                                        }
                                    </tbody>
                                </table>
                            </div>
                            {/* /.card-body */}
                       
                        {/* /.card */}
                    </div>
                </div>


            </div>

        </div>
    )
}
export default Productlist