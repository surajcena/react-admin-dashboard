import logo from './logo.svg';
import './App.css';
import React,{ lazy,Suspense }  from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Login from './components/Login';
import Register from './components/Register';
import Header from './components/Header';
import Footer from './components/Footer';
import Menu from './components/Menu';
import Dashboard from './components/Dashboard';
import Protected from './components/Protected';

import Productlist from './components/Productlist';
import Addproduct from './components/Addproduct';
import Updateproduct from './components/Updateproduct';
import Tabletest from './components/Tabletest';
import Searchproduct from './components/Searchproduct'


// const Productlist = lazy(() => import('./components/Productlist'));

function App() {
  return (
    <div class="wrapper">
       

      <BrowserRouter>
      
        <Switch>
        <Route exact path='/' component={Tabletest} />
            {
              localStorage.getItem('user-info') ?
              <>
            <Route path="/dashboard">
            <Protected Cmt={Header} />
            <Protected Cmt={Menu} />
            <Protected Cmt={Dashboard} />
            <Protected Cmt={Footer} />
            </Route>
            <Route path="/productlist">
            <Protected Cmt={Productlist}/>
            </Route>
            <Route path="/add">
            <Protected Cmt={Addproduct}/>
            </Route>
            <Route path="/search">
            <Protected Cmt={Searchproduct}/>
            </Route>
            <Route  path="/update/:id">
            <Protected Cmt={Updateproduct}/>
            </Route>
           
            </>
            :
            
            <>
          <Route path="/login">
            <Login />
          </Route>
          
          
          <Route path="/register">
            <Register />
          </Route>
          
         
            </>
          

            }
          
        </Switch>
       
        {/* switch le sabai vanda paila aaune route lai dekhauxa yo use vayena vane header 2 ota dekhinxan */}
      </BrowserRouter>


    </div>

  );
}

export default App;
